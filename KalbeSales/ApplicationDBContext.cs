﻿using KalbeSales.Models;
using Microsoft.EntityFrameworkCore;

namespace KalbeSales
{
    public class ApplicationDBContext : DbContext
    {

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options) { }

        public DbSet<Product> Product { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Sales> Sales { get; set; }

    }
}
