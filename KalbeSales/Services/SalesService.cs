﻿using KalbeSales;
using KalbeSales.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

public class SalesService
{
    private readonly ApplicationDBContext _dbContext;

    public SalesService(ApplicationDBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public List<Sales> GetAllSales()
    {
        try
        {
            List<Sales> sales = _dbContext.Sales.ToList();
            return sales;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public Sales GetSalesById(int salesId)
    {
        var sales = _dbContext.Sales.Find(salesId);
        return sales;
    }

    public void AddSales(Sales sales)
    {
        _dbContext.Sales.Add(sales);
        _dbContext.SaveChanges();
    }

    public void UpdateSales(Sales updatedSales)
    {
        var existingSales = _dbContext.Sales.Find(updatedSales.SalesId);
        if (existingSales != null)
        {
            existingSales.CustomerId = updatedSales.CustomerId;
            existingSales.ProductId = updatedSales.ProductId;
            existingSales.DateOrder = updatedSales.DateOrder;
            existingSales.Quantity = updatedSales.Quantity;

            _dbContext.SaveChanges();
        }
    }

    public void DeleteSales(int salesId)
    {
        var salesToDelete = _dbContext.Sales.Find(salesId);
        if (salesToDelete != null)
        {
            _dbContext.Sales.Remove(salesToDelete);
            _dbContext.SaveChanges();
        }
    }
}
