﻿using KalbeSales;
using KalbeSales.Models;
using System;
using System.Collections.Generic;

public class ProductService
{
    private readonly ApplicationDBContext _dbContext;
    public ProductService(ApplicationDBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public List<Product> GetAllProducts()
    {
        try
        {
            List<Product> products = new List<Product>();
            products = _dbContext.Product.ToList();
            return products;
        }
        catch (Exception)
        {

            throw;
        }

    }

    public Product GetProductById(int productId)
    {
        var product = _dbContext.Product.Find(productId);
        return product;
    }

    public void AddProduct(Product product)
    {
        _dbContext.Product.Add(product);
        _dbContext.SaveChanges();
    }

    public void UpdateProduct(Product updatedProduct)
    {
        var existingProduct = _dbContext.Product.Find(updatedProduct.ProductID);
        if (existingProduct != null)
        {
            existingProduct.ProductCode = updatedProduct.ProductCode;
            existingProduct.ProductName = updatedProduct.ProductName;
            existingProduct.Quantity = updatedProduct.Quantity;
            existingProduct.Price = updatedProduct.Price;
            existingProduct.InsertedAt = DateTime.Now;

            _dbContext.SaveChanges();
        }
    }

    public void DeleteProduct(int productId)
    {
        var productToDelete = _dbContext.Product.Find(productId);
        if (productToDelete != null)
        {
            _dbContext.Product.Remove(productToDelete);
            _dbContext.SaveChanges();
        }
    }

}
