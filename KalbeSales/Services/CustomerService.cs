﻿using KalbeSales;
using KalbeSales.Models;
using System;
using System.Collections.Generic;

public class CustomerService
{
    private readonly ApplicationDBContext _dbContext;

    public CustomerService(ApplicationDBContext dbContext)
    {
        _dbContext = dbContext;
    }

    public List<Customer> GetAllCustomers()
    {
        try
        {
            List<Customer> customers = new List<Customer>();
            customers = _dbContext.Customer.ToList();
            return customers;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public Customer GetCustomerById(int customerId)
    {
        var customer = _dbContext.Customer.Find(customerId);
        return customer;
    }

    public void AddCustomer(Customer customer)
    {
        _dbContext.Customer.Add(customer);
        _dbContext.SaveChanges();
    }

    public void UpdateCustomer(Customer updatedCustomer)
    {
        var existingCustomer = _dbContext.Customer.Find(updatedCustomer.CustomerId);
        if (existingCustomer != null)
        {
            existingCustomer.CustomerName = updatedCustomer.CustomerName;
            existingCustomer.CustomerAddress = updatedCustomer.CustomerAddress;
            existingCustomer.Gender = updatedCustomer.Gender;
            existingCustomer.TanggalLahir = updatedCustomer.TanggalLahir;
            existingCustomer.InsertedAt = DateTime.Now;

            _dbContext.SaveChanges();
        }
    }

    public void DeleteCustomer(int customerId)
    {
        var customerToDelete = _dbContext.Customer.Find(customerId);
        if (customerToDelete != null)
        {
            _dbContext.Customer.Remove(customerToDelete);
            _dbContext.SaveChanges();
        }
    }
}
