﻿using KalbeSales.Models;
using KalbeSales.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace KalbeSales.Controllers
{
    public class CustomerController : Controller
    {
        private readonly CustomerService _customerService;

        public CustomerController(CustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            var customers = _customerService.GetAllCustomers().Select(c => new CustomerViewModel
            {
                CustomerId = c.CustomerId,
                CustomerName = c.CustomerName,
                CustomerAddress = c.CustomerAddress,
                Gender = c.Gender,
                TanggalLahir = c.TanggalLahir
            });

            return View(customers);
        }

        public IActionResult Create()
        {
            var customer = new Customer();

            return View("CreateEdit", customer);
        }

        [HttpPost]
        public IActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                _customerService.AddCustomer(customer);
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        public IActionResult Edit(int id)
        {
            var customer = _customerService.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound();
            }
            return View("CreateEdit", customer);
        }

        [HttpPost]
        public IActionResult Edit(Customer updatedCustomer)
        {
            if (ModelState.IsValid)
            {
                _customerService.UpdateCustomer(updatedCustomer);
                return RedirectToAction("Index");
            }
            return View(updatedCustomer);
        }

        public IActionResult Delete(int id)
        {
            var customer = _customerService.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound();
            }
            _customerService.DeleteCustomer(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteConfirmed(int id)
        {
            _customerService.DeleteCustomer(id);
            return RedirectToAction("Index");
        }
    }
}
