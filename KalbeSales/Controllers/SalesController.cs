﻿using KalbeSales.Models;
using KalbeSales.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace KalbeSales.Controllers
{
    public class SalesController : Controller
    {
        private readonly SalesService _salesService;
        private readonly ProductService _productService;
        private readonly CustomerService _customerService;

        public SalesController(SalesService salesService, ProductService productService, CustomerService customerService)
        {
            _salesService = salesService;
            _productService = productService;
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            // Retrieve and map data to SalesViewModel
            var sales = _salesService.GetAllSales().Select(s => new SalesViewModel
            {
                SalesId = s.SalesId,
                CustomerId = s.CustomerId,
                CustomerName = _customerService.GetCustomerById(s.CustomerId)?.CustomerName,
                ProductId = s.ProductId,
                ProductName = _productService.GetProductById(s.ProductId)?.ProductName,
                DateOrder = s.DateOrder,
                Quantity = s.Quantity
            });

            return View(sales);
        }

        public IActionResult Create()
        {

            var products = _productService.GetAllProducts()
                .Select(p => new SelectListItem
                {
                    Value = p.ProductID.ToString(),
                    Text = p.ProductName
                })
                .ToList();

            products.Insert(0, new SelectListItem
            {
                Value = "",
                Text = "Select Product"
            });


            ViewBag.Products = products;

            var customers = _customerService.GetAllCustomers()
                .Select(p => new SelectListItem
                {
                    Value = p.CustomerId.ToString(),
                    Text = p.CustomerName
                })
                .ToList();

            customers.Insert(0, new SelectListItem
            {
                Value = "",
                Text = "Select Customer"
            });

            ViewBag.Customers = customers;

            var sale = new Sales();

            return View("CreateEdit", sale);
        }



        [HttpPost]
        public IActionResult Create(Sales sales)
        {
            if (ModelState.IsValid)
            {
                _salesService.AddSales(sales);
                return RedirectToAction("Index");
            }
            return View("CreateEdit", sales);
        }

        public IActionResult Edit(int id)
        {
            var sale = _salesService.GetSalesById(id);

            if (sale == null)
            {
                return NotFound();
            }

            // Create a list of SelectListItem for products
            var products = _productService.GetAllProducts()
                .Select(p => new SelectListItem
                {
                    Value = p.ProductID.ToString(),
                    Text = p.ProductName
                })
                .ToList();

            // Create a list of SelectListItem for customers
            var customers = _customerService.GetAllCustomers()
                .Select(c => new SelectListItem
                {
                    Value = c.CustomerId.ToString(),
                    Text = c.CustomerName
                })
                .ToList();

            // Assign the lists to ViewBag
            ViewBag.Products = products;
            ViewBag.Customers = customers;

            return View("CreateEdit", sale);
        }


        [HttpPost]
        public IActionResult Edit(Sales updatedSales)
        {
            if (ModelState.IsValid)
            {
                _salesService.UpdateSales(updatedSales);
                return RedirectToAction("Index");
            }
            return View("CreateEdit", updatedSales);
        }

        public IActionResult Delete(int id)
        {
            var sales = _salesService.GetSalesById(id);
            if (sales == null)
            {
                return NotFound();
            }
            _salesService.DeleteSales(id);
            return RedirectToAction("Index");
        }


        [HttpPost]
        public IActionResult DeleteConfirmed(int id)
        {
            _salesService.DeleteSales(id);
            return RedirectToAction("Index");
        }
    }
}
