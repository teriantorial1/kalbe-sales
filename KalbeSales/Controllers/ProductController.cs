﻿using KalbeSales.Models;
using KalbeSales.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace KalbeSales.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductService _productService;

        public ProductController(ProductService productService)
        {
            _productService = productService;
        }

        public IActionResult Index()
        {
            // Retrieve and map data to ProductViewModel
            var products = _productService.GetAllProducts().Select(p => new ProductViewModel
            {
                ProductID = p.ProductID,
                ProductCode = p.ProductCode,
                ProductName = p.ProductName,
                Price = p.Price,
                Quantity = p.Quantity
    
            });

            return View(products);
        }

        public IActionResult Create()
        {
           
            var product = new Product();

            return View("CreateEdit", product);
        }


        [HttpPost]
        public IActionResult Create(Product product)
        {
            if (ModelState.IsValid)
            {
                _productService.AddProduct(product);
                return RedirectToAction("Index");
            }
            return View(product);
        }

        public IActionResult Edit(int id)
        {
            var product = _productService.GetProductById(id);
            if (product == null)
            {
                return NotFound();
            }
            return View("CreateEdit", product);
        }

        [HttpPost]
        public IActionResult Edit(Product updatedProduct)
        {
            if (ModelState.IsValid)
            {
                _productService.UpdateProduct(updatedProduct);
                return RedirectToAction("Index");
            }
            return View(updatedProduct);
        }

        public IActionResult Delete(int id)
        {
            var product = _productService.GetProductById(id);
            if (product == null)
            {
                return NotFound();
            }
            _productService.DeleteProduct(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteConfirmed(int id)
        {
            _productService.DeleteProduct(id);
            return RedirectToAction("Index");
        }
    }

}