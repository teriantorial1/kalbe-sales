﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KalbeSales.Models
{
    public class Sales
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SalesId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public DateTime DateOrder { get; set; }
        public int Quantity { get; set; }


    }
}
