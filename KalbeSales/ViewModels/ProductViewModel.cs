﻿namespace KalbeSales.ViewModels
{
    public class ProductViewModel
    {
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public DateTime InsertedAt { get; set; }
    }
}
