﻿namespace KalbeSales.ViewModels
{
    public class SalesViewModel
    {
        public int SalesId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public DateTime DateOrder { get; set; }
        public int Quantity { get; set; }
    }
}
