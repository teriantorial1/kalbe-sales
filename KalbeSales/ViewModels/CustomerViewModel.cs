﻿namespace KalbeSales.ViewModels
{
    public class CustomerViewModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public bool Gender { get; set; }
        public DateTime TanggalLahir { get; set; }
        public DateTime InsertedAt { get; set; }
    }
}
